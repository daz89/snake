import React from "react";
import { GameLoopProvider } from "./GameLoop";
import { GameStateProvider } from "./GameState";
import "./styles.css";

import Snake from "./Snake";
import Block from "./Block";

function App() {
  return (
    <GameLoopProvider fps={10}>
      <GameStateProvider>
        {/* <Snake /> */}
        <div style={{ padding: "100px" }}>
          <div>
            <Block />
          </div>
        </div>
      </GameStateProvider>
    </GameLoopProvider>
  );
}

export default App;
