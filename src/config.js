export const BOARD_SIZE = 25;
export const NUM_TILES = Math.pow(BOARD_SIZE, 2);
export const STARTING_LENGTH = 10;
