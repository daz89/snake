import React, { useCallback, useEffect, useRef } from "react";

type Callback = () => void;
type CtxState = {
  subscribe(cb: Callback): void;
};
type GameLoopProps = {
  children: React.ReactNode;
  fps: number;
};

const GameLoopContext = React.createContext<CtxState | undefined>(undefined);

function GameLoopProvider({ children, fps }: GameLoopProps) {
  const subscribers = useRef<Array<Callback>>([]);
  const prevTime = useRef<number>(performance.now());

  function subscribe(cb: Callback) {
    subscribers.current.push(cb);
    return function unsubscribe() {
      subscribers.current = subscribers.current.filter(s => s !== cb);
    };
  }

  const loop = useCallback(
    (curTime: number) => {
      const frameLength = 1000 / fps;
      const delta = curTime - prevTime.current;
      if (delta >= frameLength) {
        performUpdates();
        prevTime.current = curTime;
      }
      window.requestAnimationFrame(loop);

      function performUpdates() {
        if (subscribers.current.length) {
          for (let s of subscribers.current) {
            s();
          }
        }
      }
    },
    [fps]
  );

  useEffect(() => {
    window.requestAnimationFrame(loop);
  }, [loop]);

  return (
    <GameLoopContext.Provider value={{ subscribe }}>
      {children}
    </GameLoopContext.Provider>
  );
}

function useGameLoop() {
  const ctx = React.useContext(GameLoopContext);
  if (ctx === undefined) {
    throw new Error("useGameLoop must be used within a GameLoopProvider");
  }
  return ctx;
}

export { GameLoopContext, GameLoopProvider, useGameLoop };
