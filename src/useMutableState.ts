import { useState, useRef, useCallback } from "react";

type NewStateGenerator<T> = (newState: T) => Partial<T>;

export default function useMutableState<T>(
  initialState: T
): [T, (cb: NewStateGenerator<T>) => void] {
  const [, setState] = useState<T>(initialState);
  const mutableState = useRef<T>(initialState);

  const update = useCallback(
    (cb: NewStateGenerator<T>) => {
      const updatedState = {
        ...mutableState.current,
        ...cb(mutableState.current)
      };

      mutableState.current = updatedState;
      setState(updatedState);
    },
    [mutableState]
  );

  return [mutableState.current, update];
}
