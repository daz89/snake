import React, {
  useEffect,
  useRef,
  useContext,
  useCallback,
  createContext
} from "react";
import useMutableState from "./useMutableState";
import { BOARD_SIZE, NUM_TILES, STARTING_LENGTH } from "./config";

export enum TileType {
  EMPTY = "EMPTY",
  HEAD = "HEAD",
  BODY = "BODY",
  TAIL = "TAIL",
  FOOD = "FOOD"
}

export enum Direction {
  LEFT = "LEFT",
  RIGHT = "RIGHT",
  UP = "UP",
  DOWN = "DOWN"
}

export enum GameStatus {
  IDLE,
  RUNNING,
  OVER
}

type Tiles = number[];
type Coord = { x: number; y: number };

type State = {
  snakePos: Tiles;
  foodPos: number;
  direction: Direction;
  gameStatus: GameStatus;
  board: TileType[];
  isGameOver: boolean;
};

function getInitialSnakePos() {
  const start = Math.ceil(NUM_TILES / 2 - STARTING_LENGTH / 2);
  return new Array(STARTING_LENGTH).fill(null).map((_, index) => start + index);
}

function generateFoodPos(prevPos: number, snakePos: Tiles): number {
  const takenTiles = [prevPos, ...snakePos];
  const freeTiles = Array(NUM_TILES)
    .fill(null)
    .map((t, i) => i + 1)
    .filter(t => takenTiles.indexOf(t) === -1);
  return freeTiles[Math.floor(Math.random() * NUM_TILES) + 1];
}

function tileToCoord(tile: number): Coord {
  return {
    x: tile % BOARD_SIZE,
    y: Math.floor(tile / BOARD_SIZE)
  };
}

function coordToTile({ x, y }: Coord): number {
  return y * BOARD_SIZE + x;
}

function getNextHeadCoord(curHeadTile: number, direction: Direction): Coord {
  let nextHeadCoord = tileToCoord(curHeadTile);
  switch (direction) {
    case Direction.LEFT:
      nextHeadCoord.x--;
      break;
    case Direction.RIGHT:
      nextHeadCoord.x++;
      break;
    case Direction.UP:
      nextHeadCoord.y--;
      break;
    case Direction.DOWN:
      nextHeadCoord.y++;
      break;
  }
  return nextHeadCoord;
}

function getNextSnakePos(curPos: Tiles, direction: Direction): Tiles {
  const curHeadTile = curPos[0];
  const nextHeadTile = coordToTile(getNextHeadCoord(curHeadTile, direction));
  let initial: Tiles = [];
  const nextPos: number[] = curPos.reduce((pos, nextTile, index, array) => {
    // Handle the head tile seperately
    if (index) {
      pos[index] = array[index - 1];
    }
    return pos;
  }, initial);
  nextPos[0] = nextHeadTile;
  return nextPos;
}

function isGameOver(curPos: Tiles, direction: Direction) {
  const nextHeadCoord = getNextHeadCoord(curPos[0], direction);
  const nextHeadTile = coordToTile(nextHeadCoord);
  const nextPos = getNextSnakePos(curPos, direction);
  const [, ...body] = nextPos;
  return (
    nextHeadCoord.x < 0 ||
    nextHeadCoord.y < 0 ||
    nextHeadCoord.x > BOARD_SIZE - 1 ||
    nextHeadCoord.y > BOARD_SIZE - 1 ||
    body.indexOf(nextHeadTile) > -1
  );
}

function getBoard(snakePos: Tiles, foodPos: number): TileType[] {
  const [headPos, ...body] = snakePos;
  let bodyIndex = 0;

  body.sort((a, b) => a - b);

  return Array(NUM_TILES)
    .fill(null)
    .map((t, i) => {
      switch (true) {
        case i === headPos:
          return TileType.HEAD;
        case i === foodPos:
          return TileType.FOOD;
        case i === body[bodyIndex]:
          bodyIndex++;
          return TileType.BODY;
        default:
          return TileType.EMPTY;
      }
    });
}

function initialState(): State {
  const snakePos = getInitialSnakePos();
  const foodPos = generateFoodPos(-1, snakePos);
  return {
    snakePos,
    foodPos,
    direction: Direction.LEFT,
    gameStatus: GameStatus.RUNNING,
    board: getBoard(snakePos, foodPos),
    isGameOver: false
  };
}

const GameStateContext = createContext<{
  state: State;
  update: () => void;
}>({
  state: initialState(),
  update: () => { }
});

export function GameStateProvider({ children }: { children: React.ReactNode }) {
  const [state, updateState] = useMutableState<State>(initialState());
  const queuedDirection = useRef<Direction[]>([]);

  const update = React.useCallback(() => {
    updateState(s => {
      const newSnakePos = getNextSnakePos(s.snakePos, s.direction);
      const nextDirection = queuedDirection.current.shift() || s.direction;
      const gameOver = isGameOver(s.snakePos, s.direction);

      if (gameOver) {
        return {
          isGameOver: true
        };
      }

      return {
        ...s,
        snakePos: newSnakePos,
        direction: nextDirection,
        board: getBoard(newSnakePos, s.foodPos)
      };
    });
  }, [updateState]);

  const onKeyDown = useCallback((e: KeyboardEvent) => {
    let direction;
    const lastAddedDirection = queuedDirection.current[queuedDirection.current.length - 1];
    switch (e.key) {
      case "ArrowLeft":
        if (lastAddedDirection === Direction.RIGHT) {
          return;
        }
        direction = Direction.LEFT;
        break;
      case "ArrowRight":
        if (lastAddedDirection === Direction.LEFT) {
          return;
        }
        direction = Direction.RIGHT;
        break;
      case "ArrowUp":
        if (lastAddedDirection === Direction.DOWN) {
          return;
        }
        direction = Direction.UP;
        break;
      case "ArrowDown":
        if (lastAddedDirection === Direction.UP) {
          return;
        }
        direction = Direction.DOWN;
        break;
      default:
        direction = null;
    }

    if (direction) {
      queuedDirection.current.push(direction);
    }
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", onKeyDown);
    return () => {
      document.removeEventListener("keydown", onKeyDown);
    };
  });

  return (
    <GameStateContext.Provider value={{ state, update }}>
      {children}
    </GameStateContext.Provider>
  );
}

export function useGameState() {
  const ctx = useContext(GameStateContext);
  if (ctx === undefined) {
    throw new Error("useGameState must be used within a GameStateProvider");
  }
  return ctx;
}
