import React, { useEffect } from "react";
import { BOARD_SIZE } from "./config";
import { useGameLoop } from "./GameLoop";
import { useGameState, TileType } from "./GameState";

function Tile({ type }: { type: TileType }) {
  let className = '';

  switch (type) {
    case TileType.HEAD:
      className = "tile--head";
      break;
    case TileType.BODY:
      className = "tile--body";
      break;
    case TileType.FOOD:
      className = "tile--food";
      break;
  }

  if (!className) {
    return <div className="tile-blank" />
  }

  return (
    <div className="tile-container">
      <div className={`tile ${className}`}>
        <div className="front" />
        <div className="back" />
        <div className="top" />
        <div className="bottom" />
        <div className="left" />
        <div className="right" />
      </div>
    </div>
  );
}

function Tiles({ board }: { board: TileType[] }) {
  const style = { gridTemplateColumns: `repeat(${BOARD_SIZE}, auto)` };
  return (
    <div className="tiles" style={style}>
      {board.map((t, i) => (
        <Tile type={t} key={i} />
      ))}
    </div>
  );
}

function Snake() {
  const { subscribe } = useGameLoop();
  const { state, update } = useGameState();

  useEffect(() => {
    subscribe(update);
  }, [subscribe, update]);

  return (
    <div className="board-container">
      <div className="board">
        <Tiles board={state.board} />
      </div>
    </div>
  );
}

export default Snake;
