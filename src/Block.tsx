import React from "react";

const Block = ({ className }: { className?: string }) => (
    <div className="block-container" >
        <div className={`block ${className}`}>
            <div className="front" />
            <div className="back" />
            <div className="top" />
            <div className="bottom" />
            <div className="left" />
            <div className="right" />
        </div>
    </div>
)

export default Block